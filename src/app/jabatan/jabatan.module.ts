import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { JabatanRoutes } from './jabatan.routing';
import { JabatanComponent } from './jabatan.component';
import { DemoMaterialModule } from '../demo-material-module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(JabatanRoutes),
    DemoMaterialModule
  ],
  declarations: [JabatanComponent]
})
export class JabatanModule { }