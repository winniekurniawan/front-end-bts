import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { karyawan } from '../model/karyawan';
import {login} from '../model/login';



const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = "http://3.0.56.213:3001/";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getProducts (): Observable<karyawan[]> {
    return this.http.get<karyawan[]>('http://private-caa4d6-aaww.apiary-mock.com/questions' )
      .pipe(
        tap(products => console.log('Fetch products')),
        catchError(this.handleError('getProducts', []))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


  addProduct (product): Observable<karyawan> {
    return this.http.post<karyawan>('https://private-caa4d6-aaww.apiary-mock.com/questions', product, httpOptions).pipe(
      tap((product: karyawan) => console.log(`added product w/ id=${product.NIP}`)),
      catchError(this.handleError<karyawan>('addProduct'))
    );
  }

  // get by id
  getProduct(id: string): Observable<karyawan> {
    const url = `${'https://private-caa4d6-aaww.apiary-mock.com/questions'}/${id}`;
    return this.http.get<karyawan>(url).pipe(
      tap(_ => console.log(`fetched product id=${id}`)),
      catchError(this.handleError<karyawan>(`getProduct id=${id}`))
    );
  }

  checkLogin(email : string, password : string) : Observable<login>{
    const url = `${'http://polls.apiblueprint.org/loginForm/email/pasword'}/${email},${password}`;
    return this.http.get<login>(url).pipe(
      tap(_ => console.log(`fetched product id=${email},${password}`)),
      catchError(this.handleError<login>(`getProduct id=${email},${password}`))
    );
  }

}