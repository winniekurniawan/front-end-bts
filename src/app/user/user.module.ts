import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { UserRoutes } from './user.routing';
import { UserComponent } from './user.component';
import { DemoMaterialModule } from '../demo-material-module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserRoutes),
    DemoMaterialModule
  ],
  declarations: [UserComponent]
})
export class UserModule { }