import { Routes } from '@angular/router';

import { KarirKaryawanComponent } from './karir-karyawan.component';

export const KarirKaryawanRoutes: Routes = [{
  path: '',
  component: KarirKaryawanComponent
}];
