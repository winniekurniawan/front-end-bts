import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { KarirKaryawanComponent } from './karir-karyawan.component';
import { KarirKaryawanRoutes } from './karir-karyawan.routing';
import { DemoMaterialModule } from '../demo-material-module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(KarirKaryawanRoutes),
    DemoMaterialModule
  ],
  declarations: [KarirKaryawanComponent]
})
export class KarirKaryawanModule { }