import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';

// modal
import { setTheme } from 'ngx-bootstrap/utils';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal'; 

// tabel
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { ApiService } from '../services/api.service';
import { karyawan } from '../model/karyawan';

import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}

/** Constants used to fill up our data base. */
const COLORS: string[] = ['maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple',
  'fuchsia', 'lime', 'teal', 'aqua', 'blue', 'navy', 'black', 'gray'];
const NAMES: string[] = ['Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack',
  'Charlotte', 'Theodore', 'Isla', 'Oliver', 'Isabella', 'Jasper',
  'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'];

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
//modal
modalRef: BsModalRef;
message: string;

// tabel
displayedColumns: string[] = ['email'];
dataSource: MatTableDataSource<karyawan>;
  sort: MatSort;
  paginator: MatPaginator;
  isLoadingResults: boolean;
  router: any;
  karyawan: karyawan;

//tabel
// @ViewChild(MatPaginator) paginator: MatPaginator;
// @ViewChild(MatSort) sort: MatSort;

@ViewChild(MatSort) set matSort(ms: MatSort) {
  this.sort = ms;
  this.setDataSourceAttributes();
}

@ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
  this.paginator = mp;
  this.setDataSourceAttributes();
}

productForm: FormGroup;
editKaryawan: FormGroup;
loginForm : FormGroup;

//get all karyawan
data: karyawan[] = [];

constructor(private modalService: BsModalService, private api: ApiService, private formbuilder:FormBuilder) { 
  // modal
  setTheme('bs4');

  // tabel
  // Create 100 users
  // const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));

  // // Assign the data to the data source for the table to render
  // this.dataSource = new MatTableDataSource(users);
}

ngOnInit() {
  this.getAllKaryawan();

  // this.dataSource.paginator = this.paginator;
  // this.dataSource.sort = this.sort;

  this.loginForm = this.formbuilder.group({
    'email' : ['', [Validators.required, Validators.email]],
    'password' : ['', [Validators.required]]
  });
}

setDataSourceAttributes() {
  if (this.paginator && this.sort) {
    // this.applyFilter('');
  }
}

onFormSubmit(form:NgForm) {
  this.isLoadingResults = true;
  this.api.addProduct(form)
    .subscribe(res => {
        // let id = res['_id'];
        this.isLoadingResults = false;
        this.modalRef.hide();
        
        // this.router.navigate(['/product-details', id]);
      }, (err) => {
        console.log(err);
        this.isLoadingResults = false;
      });
}

getAllKaryawan ()
{
  this.api.getProducts()
      .subscribe(res => {
        this.data = res;
        // this.resultsLength = this.data.resultsLength;
        this.dataSource = new MatTableDataSource<karyawan>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        console.log(this.data);
      }, err => {
        console.log(err);
      });
}

openModal(template: TemplateRef<any>) {
  this.modalRef = this.modalService.show(template);
}

openModalById (template: TemplateRef<any>, NIP: string)
{
  this.api.getProduct( NIP )
      .subscribe(res => {
        this.karyawan = res;
        // panggil modal
        this.modalRef = this.modalService.show(template);
        console.log(this.karyawan);
      }, err => {
        console.log(err);
      });
}

// modal cofirm delete
confirm(): void {
  this.message = 'Confirmed!';
  this.modalRef.hide();
}

decline(): void {
  this.message = 'Declined!';
  this.modalRef.hide();
}

// tabel
applyFilter(filterValue: string) {
  this.dataSource.filter = filterValue.trim().toLowerCase();

  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}
}

/** Builds and returns a new User. */
function createNewUser(id: number): UserData {
const name =
    NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
    NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

return {
  id: id.toString(),
  name: name,
  progress: Math.round(Math.random() * 100).toString(),
  color: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
};
}

