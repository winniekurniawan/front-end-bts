import { Injectable } from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon?: string;
  children?: ChildrenItems[];
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

const MENUITEMS = [
  { state: 'dashboard', name: 'Halaman Utama', type: 'link', icon: 'home' },
  { state: 'karyawan', type: 'link', name: 'Karyawan', icon: 'people' },
  { state: '', type: 'sub', name: 'Struktur', icon: 'device_hub', 
    children: [
      {
        state: 'karir-karyawan', type: 'link', name: 'Karir Karyawan'
      },
      {
        state: 'divisi', type: 'link', name: 'Divisi'
      },
      {
        state: 'departemen', type: 'link', name: 'Departement'
      },
      {
        state: 'jabatan', type: 'link', name: 'Jabatan'
      },
    ]
  },
  { state: 'user', type: 'link', name: 'User', icon: 'phone_iphone' },
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}
