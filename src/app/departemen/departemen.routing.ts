import { Routes } from '@angular/router';

import { DepartemenComponent } from './departemen.component';

export const DepartemenRoutes: Routes = [{
  path: '',
  component: DepartemenComponent
}];
