import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartemenComponent } from './departemen.component';
import { RouterModule } from '@angular/router';
import { DepartemenRoutes } from './departemen.routing';
import { DemoMaterialModule } from '../demo-material-module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DepartemenRoutes),
    DemoMaterialModule
  ],
  declarations: [DepartemenComponent]
})
export class DepartemenModule { }