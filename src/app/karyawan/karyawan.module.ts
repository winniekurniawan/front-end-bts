import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { KaryawanComponent } from './karyawan.component';
import { KaryawanRoutes } from './karyawan.routing';
import { DemoMaterialModule } from '../demo-material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(KaryawanRoutes),
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [KaryawanComponent]
})
export class KaryawanModule { }