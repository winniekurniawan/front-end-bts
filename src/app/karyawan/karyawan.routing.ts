import { Routes } from '@angular/router';

import { KaryawanComponent } from './karyawan.component';

export const KaryawanRoutes: Routes = [{
  path: '',
  component: KaryawanComponent
}];
