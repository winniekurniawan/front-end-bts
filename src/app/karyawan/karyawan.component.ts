import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';

// modal
import { setTheme } from 'ngx-bootstrap/utils';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal'; 

// tabel
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ApiService } from '../services/api.service';
import { karyawan } from '../model/karyawan';

import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';



export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}

@Component({
  selector: 'app-karyawan',
  templateUrl: './karyawan.component.html',
  styleUrls: ['./karyawan.component.css']
})
export class KaryawanComponent implements OnInit {
  //modal
  modalRef: BsModalRef;
  message: string;

  // tabel
  displayedColumns: string[] = ['NIP', 'Nama', 'email', 'Telp_Kerja','Alamat','Alamat2','act'];
  dataSource: MatTableDataSource<karyawan>;
    sort: MatSort;
    paginator: MatPaginator;
    isLoadingResults: boolean;
    router: any;
    karyawan: karyawan;

  //tabel
  @ViewChild(MatSort) set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
  }

  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  tambahKaryawan: FormGroup;
  editKaryawan: FormGroup;

  //get all karyawan
  data: karyawan[] = [];

  constructor(private modalService: BsModalService, private api: ApiService, private formbuilder:FormBuilder) { 
    // modal
    setTheme('bs4');

    // tabel
    // Create 100 users
    // const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));

    // // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(users);
  }

  ngOnInit() {
    this.getAllKaryawan();

    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;

    this.tambahKaryawan = this.formbuilder.group({
      'Nama' : ['', Validators.required],
      'NIP' : ['', Validators.required],
      'email' : ['', [Validators.required, Validators.email]],
      'Tempat_Lahir' : ['', Validators.required],
      'Tanggal_Lahir' : ['', Validators.required],
      'Alamat' : ['', Validators.required],
      'NPWP' : ['', Validators.required],
      'BPJS_TenagaKerja' : ['', Validators.required],
      'BPJS_Kesehatan' : ['', Validators.required],
      'Id_Divisi' : ['', Validators.required],
      'Id_Departemen' : ['', Validators.required],
      'Id_Jabatan' : ['', Validators.required],
      'Alamat_Kerja' : ['', Validators.required],
      'Telp_Kerja' : ['', Validators.required]

    });

    this.editKaryawan = this.formbuilder.group({
      'Nama' : ['', Validators.required],
      'NIP' : ['', Validators.required],
      'email' : ['', [Validators.required, Validators.email]],
      'Tempat_Lahir' : ['', Validators.required],
      'Tanggal_Lahir' : ['', Validators.required],
      'Alamat' : ['', Validators.required],
      'NPWP' : ['', Validators.required],
      'BPJS_TenagaKerja' : ['', Validators.required],
      'BPJS_Kesehatan' : ['', Validators.required],
      'Id_Divisi' : ['', Validators.required],
      'Id_Departemen' : ['', Validators.required],
      'Id_Jabatan' : ['', Validators.required],
      'Alamat_Kerja' : ['', Validators.required],
      'Telp_Kerja' : ['', Validators.required]

    });
  }

  setDataSourceAttributes() {
    if (this.paginator && this.sort) {
      // this.applyFilter('');
    }
  }

  onFormSubmit(form:NgForm) {
    this.isLoadingResults = true;
    this.api.addProduct(form)
      .subscribe(res => {
          // let id = res['_id'];
          this.isLoadingResults = false;
          this.modalRef.hide();
          
          // this.router.navigate(['/product-details', id]);
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        });
  }

  getAllKaryawan ()
  {
    this.api.getProducts()
        .subscribe(res => {
          this.data = res;
          // this.resultsLength = this.data.resultsLength;
          this.dataSource = new MatTableDataSource<karyawan>(this.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          console.log(this.data);
        }, err => {
          console.log(err);
        });
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  openModalById (template: TemplateRef<any>, NIP: string)
  {
    this.api.getProduct( NIP )
        .subscribe(res => {
          this.karyawan = res;
          // panggil modal
          this.modalRef = this.modalService.show(template);
          console.log(this.karyawan);
        }, err => {
          console.log(err);
        });
  }

  // modal cofirm delete
  confirm(): void {
    this.message = 'Confirmed!';
    this.modalRef.hide();
  }

  decline(): void {
    this.message = 'Declined!';
    this.modalRef.hide();
  }

  // tabel
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

/** Builds and returns a new User. */
// function createNewUser(id: number): UserData {
// const name =
//     NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
//     NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

// return {
//   id: id.toString(),
//   name: name,
//   progress: Math.round(Math.random() * 100).toString(),
//   color: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
// };
// }

