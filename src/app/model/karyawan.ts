export interface karyawan {
    NIP: string;
    Nama: string;
    email: string;
    Tempat_Lahir: string;
    Tanggal_Lahir: string;
    Alamat: string;
    NPWP: string;
    BPJS_TenagaKerja: string;
    BPJS_Kesehatan: string;
    Create_at: string;
    Update_at?: string | null;
    Delete_at?: null;
    Foto: string;
    Id_Divisi: number;
    Id_Departemen: number;
    Id_Jabatan: number;
    Alamat_Kerja: string;
    Telp_Kerja: string;
  }  
