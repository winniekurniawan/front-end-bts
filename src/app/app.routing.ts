import { Routes } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';
import { BasicComponent } from './layouts/basic/basic.component';

export const AppRoutes: Routes = [
  {
    path: '',
    component: BasicComponent, 
    children: [
      {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
      },
      {
        path: 'login',
        loadChildren: './login/login.module#LoginModule'
      },
      {
        path: '',
        loadChildren:
          './material-component/material.module#MaterialComponentsModule'
      },
      {
        path: 'dashboard',
        component: FullComponent,
        loadChildren: './starter/starter.module#StarterModule'
      },
      {
        path: 'karyawan',
        component: FullComponent,
        loadChildren: './karyawan/karyawan.module#KaryawanModule'
      },
      {
        path: 'user',
        component: FullComponent,
        loadChildren: './user/user.module#UserModule'
      },
      {
        path: 'divisi',
        component: FullComponent,
        loadChildren: './divisi/divisi.module#DivisiModule'
      },
      {
        path: 'karir-karyawan',
        component: FullComponent,
        loadChildren: './karir-karyawan/karir-karyawan.module#KarirKaryawanModule'
      },
      {
        path: 'departemen',
        component: FullComponent,
        loadChildren: './departemen/departemen.module#DepartemenModule'
      },
      {
        path: 'jabatan',
        component: FullComponent,
        loadChildren: './jabatan/jabatan.module#JabatanModule'
      }

    ]
  }
];
