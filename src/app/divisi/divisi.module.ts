import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DivisiRoutes } from './divisi.routing';
import { DivisiComponent } from './divisi.component';
import { DemoMaterialModule } from '../demo-material-module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DivisiRoutes),
    DemoMaterialModule
  ],
  declarations: [DivisiComponent]
})
export class DivisiModule { }