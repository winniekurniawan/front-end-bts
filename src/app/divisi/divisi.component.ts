import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';

// modal
import { setTheme } from 'ngx-bootstrap/utils';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal'; 

// tabel
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}

/** Constants used to fill up our data base. */
const COLORS: string[] = ['maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple',
  'fuchsia', 'lime', 'teal', 'aqua', 'blue', 'navy', 'black', 'gray'];
const NAMES: string[] = ['Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack',
  'Charlotte', 'Theodore', 'Isla', 'Oliver', 'Isabella', 'Jasper',
  'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'];

@Component({
  selector: 'app-divisi',
  templateUrl: './divisi.component.html',
  styleUrls: ['./divisi.component.css']
})
export class DivisiComponent implements OnInit {
//modal
modalRef: BsModalRef;
message: string;

// tabel
displayedColumns: string[] = ['no', 'nama','act'];
dataSource: MatTableDataSource<UserData>;

//tabel
@ViewChild(MatPaginator) paginator: MatPaginator;
@ViewChild(MatSort) sort: MatSort;

constructor(private modalService: BsModalService) { 
  // modal
  setTheme('bs4');

  // tabel
  // Create 100 users
  const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));

  // Assign the data to the data source for the table to render
  this.dataSource = new MatTableDataSource(users);
}

ngOnInit() {
  this.dataSource.paginator = this.paginator;
  this.dataSource.sort = this.sort;
}

openModal(template: TemplateRef<any>) {
  this.modalRef = this.modalService.show(template);
}

// modal cofirm delete
confirm(): void {
  this.message = 'Confirmed!';
  this.modalRef.hide();
}

decline(): void {
  this.message = 'Declined!';
  this.modalRef.hide();
}

// tabel
applyFilter(filterValue: string) {
  this.dataSource.filter = filterValue.trim().toLowerCase();

  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}
}

/** Builds and returns a new User. */
function createNewUser(id: number): UserData {
const name =
    NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
    NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

return {
  id: id.toString(),
  name: name,
  progress: Math.round(Math.random() * 100).toString(),
  color: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
};
}

