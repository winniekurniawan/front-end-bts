import { Routes } from '@angular/router';

import { DivisiComponent } from './divisi.component';

export const DivisiRoutes: Routes = [{
  path: '',
  component: DivisiComponent
}];
